# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
require "tzinfo"
require "active_support/all"

def timezoned(time, timezone)
  Time.zone = timezone || "Asia/Jakarta"
  timezone_time = Time.zone.parse(time).utc
  timezone_time
end

# every :day, at: TZInfo::Timezone.all_country_zone_identifiers.map{|val| timezoned('14:40pm', val)} do
every :day, at: '2:46 pm' do
  runner 'script/send_birthday.rb'
end
