# Be sure to restart your server when you modify this file.

# Avoid CORS issues when API is called from the frontend app.
# Handle Cross-Origin Resource Sharing (CORS) in order to accept cross-origin AJAX requests.

# Read more: https://github.com/cyu/rack-cors

CORS_DOMAINS = %w(localhost:3000) if Rails.env.development? || Rails.env.test?

Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins CORS_DOMAINS

    resource '*',
      headers: :any,
      expose: %w(ETag Link X-Per-Page X-Page X-Total),
      methods: [:get, :post, :put, :patch, :delete, :options, :head]
  end
end
