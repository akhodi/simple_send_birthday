# Simple API send birthday

## Tech Stack

- You can see on [Gemfile](Gemfile)

## Requirements

- Ruby
- Rails
- PostgreSQL

## Setup

#### Quick Setup / Re-Setup:
```
rails db:migrate
```

#### Development Environment:
```bash
bundle install --without production
```

#### Run Web Server (puma)
```
rails s
```

#### Run Whenever:
```bash
whenever --update-crontab --set environment=development
```
