module Response
  extend ActiveSupport::Concern

  private
    def api(data, status = 200, location = nil)
      payload = {
        response: {
          status: status,
          url: request.original_url
        },
        data: data.nil? ? nil : (JSON.parse(data) rescue data)
      }

      render json: Rails.env.production? ? payload : JSON.pretty_generate(payload.as_json), status: status, location: location
    end
end