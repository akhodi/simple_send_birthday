class UsersController < ApplicationController

  include Response

  before_action :set_user, only: [:show, :update, :destroy]

  def timezoned (time, timezone)
    Time.zone = timezone || "Central Time (US & Canada)"
    timezone_time = Time.zone.parse(time).utc
    timezone_time
  end

  # GET /users
  def index
    @users = User.all

    api @users
  end

  # GET /users/:id
  def show
    api @user
  end

  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      api @user, 201
    else
      api({ message: @user.errors }, 422)
    end
  end

  # PATCH/PUT /users/:id
  def update
    return api({ message: 'User is not found' }, 422) unless @user

    if @user.update(user_params)
      api @user, 202
    else
      api({ message: @user.errors }, 422)
    end
  end

  # DELETE /users/:id
  def destroy
    return api({ message: 'User is not found' }, 422) unless @user
    
    if @user.destroy
      api({ message: 'Data was successfully destroyed' }, 204)
    else
      api({ message: @user.errors }, 422)
    end
  rescue ActiveRecord::InvalidForeignKey => exception
    api({ message: exception.message }, 422)
  end

  private
    def set_user
      @user = User.find_by(id: params[:id])
    end

    def user_params
      params.permit(:first_name, :last_name, :birth_date, :time_zone)
    end
end
